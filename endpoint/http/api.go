package http

import (
	"github.com/gorilla/mux"
)

type Routes struct {
	Root    *mux.Router
	ApiRoot *mux.Router
}

type Api struct {
	Routes *Routes
}

func InitApi() *Api {
	api := Api{
		Routes: &Routes{},
	}

	api.Routes.Root = mux.NewRouter()
	api.Routes.ApiRoot = api.Routes.Root.PathPrefix("/api").Subrouter()

	api.InitFile()

	return &api
}
