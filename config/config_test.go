package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetString(t *testing.T) {
	var cfg = []byte(`
database:
  host: 127.0.0.1
`)
	InitFromBytes(cfg)
	assert.Equal(t, GetString("database.host"), "127.0.0.1")
}
