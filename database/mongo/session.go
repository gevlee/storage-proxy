package mongo

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/gevleeog/storage-proxy/config"
	"gopkg.in/mgo.v2"
)

// Collection run operation on mongo collection
func Collection(collectionName string, fn func(*mgo.Collection) error) error {
	session, err := mgo.Dial(config.GetString("database.mongo.host"))
	if err != nil {
		logrus.WithError(err).Fatal("Cannot open session!")
		return err
	}

	defer session.Close()
	err = fn(session.DB(config.GetString("database.mongo.dbname")).C(collectionName))
	return err
}

// Database run operation on mongo collection
func Database(fn func(*mgo.Database) error) error {
	session, err := mgo.Dial(config.GetString("database.mongo.host"))
	if err != nil {
		logrus.WithError(err).Fatal("Cannot open session!")
		return err
	}

	defer session.Close()
	err = fn(session.DB(config.GetString("database.mongo.dbname")))
	return err
}
