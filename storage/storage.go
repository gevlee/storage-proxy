package storage

import (
	"io"

	"gitlab.com/gevleeog/storage-proxy/database/model"
)

func NewFileStorage() FileStorage {
	return &fileStorageFacade{
		StorageFactory: storageFactory,
	}
}

type FileStorage interface {
	SaveFile(data *model.FileData, r io.Reader) error
	GetFile(data *model.FileData) (io.Reader, error)
}

func storageFactory(storageId string) FileStorage {
	return newLocalStorage()
}

type fileStorageFacade struct {
	StorageFactory func(string) FileStorage
}

func (facade *fileStorageFacade) SaveFile(data *model.FileData, r io.Reader) error {
	return facade.StorageFactory(data.StorageType).SaveFile(data, r)
}

func (facade *fileStorageFacade) GetFile(data *model.FileData) (io.Reader, error) {
	return facade.StorageFactory(data.StorageType).GetFile(data)
}
